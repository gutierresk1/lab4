# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Contact(models.Model):    
    name = models.CharField(max_length=100)    
    phone = models.CharField(max_length=20)    
    email = models.EmailField()    
    message = models.TextField()    
    madecontact = models.BooleanField(blank=True, default=False)    
    feedback = models.TextField()
    
def __str__(self):
    return self.name