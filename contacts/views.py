# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,redirect
from .models import Contact


def contactus(request):
        if request.POST: 
            if 'name' in request.POST:                
                name = request.POST['name'] 
            if 'phone' in request.POST:               
                phone = request.POST['phone'] 
            if 'email' in request.POST:                
                email = request.POST['email'] 
            if 'message' in request.POST:                
                message = request.POST['message']
            newContact= Contact(name=name, email=email, phone=phone, message=message)
            newContact.save()
            return redirect("home")
        else:
            return render(request, "contact/contactus.html")
