from django.db import models

# Create your models here.
class Dashboard(models.Model):
    userid = models.IntegerField()
    productid= models.IntegerField()
    productname = models.CharField(max_length=100)
    phone = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    message = models.TextField()

    def __str__(self):
        return self.productname
        