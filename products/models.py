# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from .choices import COUNTRY_CHOICES
# Create your models here.
class Product(models.Model): 
    name = models.CharField(max_length=100)
    description = models.TextField()
    count = models.IntegerField("Items in Stock:")
    made= models.IntegerField("Year made")
    price = models.DecimalField("How much it should cost:", decimal_places=2, max_digits=1000) 
    used = models.BooleanField("Used Guitar") 
    dateOfPurchase = models.DateField("Put in today's date so we can keep it in our records") 
    #imagefield will be used soon..#
    customerName = models.CharField("Please enter your name:", max_length=100)
    customerEmail = models.EmailField("A valid e-mail is required") 
    customerID = models.IntegerField(primary_key=True)
    shippingAddress = models.CharField("Address Line:", max_length=1024)
    zipCode = models.CharField("Enter Zip Code:", max_length=5)
    city = models.CharField("Enter City:", max_length=100)
    #choices dropdownselect goes here#

    chooseCountry = models.CharField(max_length=13, choices = COUNTRY_CHOICES)
    photo = models.ImageField(upload_to = 'images/')

    def _str_(self): 
        return self.name + " was made in " + str(self.made) + ". The price is" + str(self.price)