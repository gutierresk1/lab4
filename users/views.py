from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib import auth

def register(request): 
    if request.POST: 
        if 'username' in request.POST: 
            username = request.POST['username']
        if 'first_name' in request.POST: 
            first_name = request.POST['first_name']
        if 'last_name' in request.POST: 
            last_name = request.POST['last_name']
        if 'email' in request.POST: 
            email = request.POST['email']
        if 'password' in request.POST: 
            password = request.POST['password']

        myuser = User(username = username, first_name = first_name, last_name = last_name, email = email)
        myuser.set_password(password)
        myuser.save()
        return redirect('home')
    else: 
        return render(request, 'userpages/register.html')

def login(request): 
    if request.POST: 
        if 'username' in request.POST:
            username = request.POST['username']
        if 'password' in request.POST: 
            password = request.POST['password']
        
        userobject = auth.authenticate(username = username, password = password)

        if userobject is None: 
            context = {"message": "Invalid credentials"}
            return render (request, "userpages/login.html", context)
        else : 
            auth.login(request, userobject)
            return redirect ('home')
    else: 
        return render(request, "userpages/login.html")

def logout(request):
    auth.logout(request)
    return redirect('home')