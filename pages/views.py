# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, get_object_or_404
from products.models import Product
# Create your views here.

def ShowHome(request):
    products = Product.objects.all()
    return render(request, "pages/home.html", {'products':products})

def ShowAbout(request):
    return render(request, "pages/about.html")
